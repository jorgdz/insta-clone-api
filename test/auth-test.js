'use strict'

const micro = require('micro')
const test = require('ava')
const listen = require('test-listen')
const fetch = require('node-fetch')

const fixtures = require('./fixtures')
const auth = require('../auth')
const utils = require('../lib/utils')
const config = require('../config')

test.beforeEach(async t => {
  const srv = micro(auth)
  t.context.url = await listen(srv)
})

test('success POST /', async t => {
  var user = fixtures.getUser()
  var url = t.context.url

  var options = {
    method: 'POST',
    body: JSON.stringify({
      username: user.username,
      password: user.password
    }),
    headers: { 'Content-Type': 'application/json' }
  }

  var response = await fetch(url, options)
  var body = await response.json()
  var decoded = await utils.verifyToken(body.token, config.secret)

  t.is(decoded.username, user.username)
})
