module.exports = {
  db: {
    host: process.env.DB_HOST || '192.168.99.100',
    port: process.env.DB_PORT || 28015,
    db: process.env.DB_NAME || 'insta-clone-db',
    setup: process.env.DB_SETUP || true
  },
  secret: process.env.INSTAGRAM_SECRET || 'instagram'
}
