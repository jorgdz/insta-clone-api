'use strict'

const micro = require('micro')
const test = require('ava')
const listen = require('test-listen')
const fetch = require('node-fetch')

const pictures = require('../pictures')
const fixtures = require('./fixtures')
const utils = require('../lib/utils')
const config = require('../config')

test.beforeEach(async t => {
  const srv = micro(pictures)
  t.context.url = await listen(srv)
})

test('GET /:id', async t => {
  var image = fixtures.getImage()
  var url = t.context.url

  var response = await fetch(`${url}/${image.publicId}`)
  var body = await response.json()

  t.deepEqual(body, image)
})

test('no token POST /', async t => {
  var image = fixtures.getImage()
  var url = t.context.url

  var options = {
    method: 'post',
    body: JSON.stringify({
      description: image.description,
      url: image.url,
      userId: image.userId
    }),
    headers: { 'Content-Type': 'application/json' }
  }

  await t.throwsAsync(async () => {
    var response = await fetch(url, options)
    await response.json()
  })
})

test('secure POST /', async t => {
  var image = fixtures.getImage()
  var url = t.context.url
  var token = await utils.signToken({ userId: image.userId }, config.secret)

  var options = {
    method: 'post',
    body: JSON.stringify({
      description: image.description,
      url: image.url,
      userId: image.userId
    }),
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  }

  var response = await fetch(url, options)
  var body = await response.json()

  t.is(response.status, 201)
  t.deepEqual(body, image)
})

test('invalid token POST /', async t => {
  var image = fixtures.getImage()
  var url = t.context.url
  var token = await utils.signToken({ userId: 'hacky' }, config.secret)

  var options = {
    method: 'post',
    body: JSON.stringify({
      description: image.description,
      url: image.url,
      userId: image.userId
    }),
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    }
  }

  await t.throwsAsync(async () => {
    var response = await fetch(url, options)
    await response.json()
  })
})

test('POST /:id/like', async t => {
  var image = fixtures.getImage()
  var url = t.context.url

  var options = {
    method: 'POST',
    body: JSON.stringify({
      username: image.userId
    }),
    headers: { 'Content-Type': 'application/json' }
  }
  var response = await fetch(`${url}/${image.id}/like`, options)
  var body = await response.json()

  var imageNew = JSON.parse(JSON.stringify(image))
  imageNew.liked = true
  imageNew.likes = 1

  t.deepEqual(body, imageNew)
})

test('GET /list', async t => {
  var images = fixtures.getImages()
  var url = t.context.url

  var response = await fetch(`${url}/list`)
  var body = await response.json()

  t.deepEqual(body, images)
})

test('GET /tag/:tag', async t => {
  var images = fixtures.getImagesByTag()
  var url = t.context.url
  var response = await fetch(`${url}/tag/awesome`)
  var body = await response.json()

  t.deepEqual(body, images)
})
