# Instagram Clone APi

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

_A rudimentary instagram clone in node js_
_I used micro as library in its version 8.0.3 because the new version expire time out when i create the server before resolve the request_

## Built with 🛠️

- [Nodejs](https://nodejs.org/en/) - Node JS
- [JS](https://developer.mozilla.org/es/docs/Web/JavaScript) - Javascript.

## Autor ✒️

- **Jorge Diaz Montoya**
