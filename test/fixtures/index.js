module.exports = {
  getImage () {
    return {
      id: '6a238b19-3ee3-4d5c-acb5-944a3c1',
      publicId: '7mDMXUb6gfXirPX1I0RxQs',
      userId: 'instagram',
      liked: false,
      likes: 0,
      url: 'http://instagram.test/7mDMXUb6gfXirPX1I0RxQs.jpg',
      description: '#awesome',
      tags: ['awesome'],
      userLikes: ['instagram'],
      createdAt: new Date().toString()
    }
  },
  getImages () {
    return [
      this.getImage(),
      this.getImage(),
      this.getImage()
    ]
  },
  getImagesByTag () {
    return [
      this.getImage(),
      this.getImage()
    ]
  },
  getUser () {
    return {
      id: 'f632db90-d6bf-46f0-9fb1-4eb6912cbdb4',
      name: 'Jorge Diaz',
      username: 'jorgdz',
      email: 'jdzm@outlook.es',
      password: '12345',
      createdAt: new Date().toString()
    }
  }
}
