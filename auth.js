'use strict'

const { send, json, sendError } = require('micro')
var HttpHash = require('http-hash')

var Db = require('insta-clone-db')
var DbStub = require('./test/stub/db')

const config = require('./config')
const utils = require('./lib/utils')

const env = process.env.NODE_ENV || 'production'

var db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('POST /', async function authenticate (req, res, params) {
  var user = await json(req)
  await db.connect()
  var auth = await db.authenticate(user.username, user.password)

  if (!auth) {
    return sendError(req, res, { error: 'invalid credentials' })
  }

  var token = await utils.signToken({ username: user.username }, config.secret)

  send(res, 200, { token: token })
})

module.exports = async function main (req, res) {
  var { method, url } = req
  var match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      // ejecutamos el handler!
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
