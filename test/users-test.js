'use strict'

const micro = require('micro')
const test = require('ava')
const listen = require('test-listen')
const fetch = require('node-fetch')

const fixtures = require('./fixtures')
const users = require('../users')

test.beforeEach(async t => {
  const srv = micro(users)
  t.context.url = await listen(srv)
})

test('POST /', async t => {
  var user = fixtures.getUser()
  var url = t.context.url

  var options = {
    method: 'POST',
    body: JSON.stringify({
      name: user.name,
      username: user.username,
      password: user.password,
      email: user.email
    }),
    headers: { 'Content-Type': 'application/json' }
  }

  var response = await fetch(url, options)
  var body = await response.json()

  delete user.email
  delete user.password

  t.is(response.status, 201)
  t.deepEqual(body, user)
})

test('GET /:username', async t => {
  var url = t.context.url
  var user = fixtures.getUser()

  var response = await fetch(`${url}/${user.username}`)
  var body = await response.json()

  delete user.email
  delete user.password

  t.deepEqual(body, user)
})
