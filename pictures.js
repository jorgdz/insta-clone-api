'use strict'

const { send, json, sendError } = require('micro')
var HttpHash = require('http-hash')

var Db = require('insta-clone-db')
var DbStub = require('./test/stub/db')

const config = require('./config')
const utils = require('./lib/utils')

const env = process.env.NODE_ENV || 'production'

var db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('GET /tag/:tag', async function byTag (req, res, params) {
  var tag = params.tag
  await db.connect()
  var images = await db.getImagesByTag(tag)
  await db.disconnect()

  send(res, 200, images)
})

hash.set('GET /list', async function list (req, res, params) {
  await db.connect()
  var images = await db.getImages()
  await db.disconnect()

  send(res, 200, images)
})

hash.set('GET /:id', async function getPicture (req, res, params) {
  var id = params.id
  await db.connect()
  var image = await db.getImage(id)
  await db.disconnect()

  send(res, 200, image)
})

hash.set('POST /', async function postPicture (req, res, params) {
  var image = await json(req)

  try {
    var token = await utils.extractToken(req)
    var encoded = await utils.verifyToken(token, config.secret)

    if (encoded && encoded.username !== image.userId) {
      return sendError(req, res, { error: 'invalid token' })
    }
  } catch (e) {
    return sendError(req, res, { error: 'invalid token' })
  }

  await db.connect()
  var created = await db.saveImage(image)
  await db.disconnect()

  send(res, 201, created)
})

hash.set('POST /:id/like', async function likePicture (req, res, params) {
  var id = params.id
  var user = await json(req)
  await db.connect()
  var image = await db.likeImage(id, user.username)
  await db.disconnect()

  send(res, 200, image)
})

hash.set('POST /:id/dislike', async function dislikePicture (req, res, params) {
  var id = params.id
  var user = await json(req)
  await db.connect()
  var image = await db.dislikeImage(id, user.username)
  await db.disconnect()

  send(res, 200, image)
})

module.exports = async (req, res) => {
  var { method, url } = req
  var match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      // ejecutamos el handler!
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
