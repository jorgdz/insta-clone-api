'use strict'

const { send, json } = require('micro')
var HttpHash = require('http-hash')
var gravatar = require('gravatar')

var Db = require('insta-clone-db')
var DbStub = require('./test/stub/db')

const config = require('./config')

const env = process.env.NODE_ENV || 'production'

var db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('GET /:username', async function getUser (req, res, params) {
  var username = params.username
  await db.connect()
  var user = await db.getUser(username)

  if (!user.facebook) {
    user.avatar = gravatar.url(user.email)
  }

  var images = await db.getImagesByUser(username)
  user.pictures = images

  delete user.email
  delete user.password

  send(res, 200, user)
})

hash.set('POST /', async function saveUser (req, res, params) {
  var user = await json(req)
  await db.connect()
  var created = await db.saveUser(user)
  await db.disconnect()

  delete created.email
  delete created.password

  send(res, 201, created)
})

module.exports = async function main (req, res) {
  var { method, url } = req
  var match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      // ejecutamos el handler!
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
